# Subprojects for the Meson Build System

This directory contains so-called wrap-files and patch directories (under packagefiles) for building and linking Siesta's dependencies with the Meson Build System.

Meson should automatically populate this directory with a folder with a dependency's source for each .wrap-file if it needs to compile that dependency.

#!/bin/sh
#
### set -x
dir=$(dirname $0)
#
echo " ===> Generating module files from templates..."

INTERFACES_FILE="Interfaces.f90"
VS_FILE="VS.uses"
V_S_FILE="V_S.uses"

execs_via_args=1
while [ $execs_via_args -eq 1 -a $# -gt 1 ] ; do
  case "$1" in 
    --kindexplorer)
      KINDS=$($2)
      shift; shift
      ;;
    --intexplorer)
      INT_KINDS=$($2)
      shift; shift
      ;;
    --interfaces-output)
      INTERFACES_FILE=$2
      shift; shift
      ;;
    --VS-output)
      VS_FILE=$2
      shift; shift
      ;;
    --V_S-output)
      V_S_FILE=$2
      shift; shift
      ;;
    *)
      execs_via_args=0
      ;;
  esac
done
if [ $execs_via_args -eq 0 ]; then
  if [ -z "$@" ] ; then
    KINDS=`./kind_explorer`
  else
    KINDS=$@
  fi
  INT_KINDS=`./int_explorer`
fi
#
echo $KINDS
rm -f *.uses $INTERFACES_FILE

for kind in ${KINDS} ; do

 echo "         USE MPI__r${kind}_V      ;  USE MPI__r${kind}_S" >> $V_S_FILE
 echo "         USE MPI__c${kind}_V      ;  USE MPI__c${kind}_S" >> $V_S_FILE

 echo "         USE MPI__r${kind}_VS      ;  USE MPI__r${kind}_SV" >> $VS_FILE
 echo "         USE MPI__c${kind}_VS     ;  USE MPI__c${kind}_SV" >> $VS_FILE

done

for kind in ${INT_KINDS} ; do

  echo "         USE MPI__i${kind}_V      ;  USE MPI__i${kind}_S" >> $V_S_FILE
  echo "         USE MPI__i${kind}_VS      ;  USE MPI__i${kind}_SV" >> $VS_FILE

done


for tag in v s sv vs ; do

for kind in ${KINDS} ; do
  sed -e "/_type/s//_r${kind}/" -e "/type/s//real(${kind})/" \
    ${dir}/mpi__type_${tag}.f90 >> $INTERFACES_FILE
  sed -e "/_type/s//_c${kind}/" -e "/type/s//complex(${kind})/" \
    ${dir}/mpi__type_${tag}.f90 >> $INTERFACES_FILE

done

for kind in ${INT_KINDS} ; do
  sed -e "/_type/s//_i${kind}/" -e "/type/s//integer(${kind})/" \
    ${dir}/mpi__type_${tag}.f90 >> $INTERFACES_FILE
done

sed -e "/_type/s//_logical/" -e "/type/s//logical/" \
    ${dir}/mpi__type_${tag}.f90 >> $INTERFACES_FILE

sed -e "/_type/s//_character/" -e "/type/s//character(*)/" \
    ${dir}/mpi__type_${tag}.f90  >> $INTERFACES_FILE

done

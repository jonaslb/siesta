# Building Siesta with Meson

Building Siesta with Meson is exceedingly simple compared to the Makefile-approach.
The only requirement is that you have blas and lapack libraries installed. You likely already do.
If you don't, you can probably install `blas-devel` and `lapack-devel` (or `libblas-dev` and `liblapack-dev`) directly in your package manager.

To build Siesta, you simply execute `meson build && ninja -C build`. Watch as Siesta is built at record-setting speeds.

To produce an optimized build, you can use the arguments `meson build -Doptimization=3 -Db_lto=true -Dfortran_args=-march=native` to Meson. Note that you should check what optimization level best fits your machine, as LTO and O3 may not be the fastest in combination. LTO is the option with the largest footprint on total compilation time.

You can enable MPI with `-Dmpi=enabled`. This requires an MPI-installation such as `openmpi` as well as `scalapack` to be installed.

Other libraries (elpa, metis, etc) are enabled automatically if Meson finds them.
Meson uses `pkgconfig` to find these libraries.
Unfortunately, some of the libraries mentioned in the Siesta manual do not appear to produce the `.pc` files needed by `pkgconfig`, so for now they cannot be used via Meson without manual intervention.
The linear algebra libraries, fdict, ncdf and elpa are known to work fine.

# Using custom library locations

If you need or want a library that isn't in your system's package repository, you'll need to find and compile the package yourself, and you'll probably install the package in some custom directory.
Some developers may even prefer this approach, as it allows use of pre-release libraries.
In any case, if the package is installed in a non-system location, `pkgconfig` cannot automatically find the libraries.
To solve this, you can guide `pkgconfig` to the correct location by settting either the `PKG_CONFIG_PATH` environment variable or by passing `--pkg-config-path ...` to Meson.
The path(s) you set should contain the `.pc` files for the libraries. Meson will then include them in Siesta automatically. You can check whether the library is found by looking at Meson's output during configuration.
If you installed a library to a custom prefix, an appropriate `PKG_CONFIG_PATH` might be `$MYPREFIX/lib/pkgconfig/`.

# Building the Utilities with Meson

There are currently no Meson build files for the utilities, so they must be built with the Makefile approach for now.

# If the build gives an error

You may have missing dependencies. Eg. `ELPA` depends (in time of writing, on this distribution) on `flexiblas`, but even if you installed ELPA with your package manager, flexiblas may not have been installed (this would be an error on the packager's behalf, but nevertheless it may happen). If you see an error such as `cannot find -lflexiblas`, then you know that you need to install `flexiblas-devel` (or `libflexiblas-dev`).
